import sys

import fitz
import nltk

sys.path.append("src/")

doc = fitz.open("./tests/exemplo.pdf")
txt = doc[0].get_text()
nltk.download("stopwords")
nltk.download("rslp")


def version_test():
    from legal_pre_processing import __version__

    assert __version__ == "0.0.6"


def test_LoadDicts():
    from legal_pre_processing.utils import LoadDicts

    _ = LoadDicts("./data/")


def test_LoadDicts_content():
    from legal_pre_processing.utils import LoadDicts

    dicts = LoadDicts("./data/")
    assert dicts.List == ["LegalRegExPatterns", "TesauroRevisado", "LegalStopwords"]


def test_LegalPreprocess():
    from legal_pre_processing.utils import LoadDicts
    from legal_pre_processing.legal_pre_processing import LegalPreprocess

    dicts = LoadDicts("./data/")
    _ = LegalPreprocess(
        domain_stopwords=dicts.LegalStopwords,
        tesauro=dicts.TesauroRevisado,
        regex_pattern=dicts.LegalRegExPatterns,
    )


def test_LegalPreprocess_ProcessText():
    from legal_pre_processing.utils import LoadDicts
    from legal_pre_processing.legal_pre_processing import LegalPreprocess

    dicts = LoadDicts("./data/")
    model = LegalPreprocess(
        domain_stopwords=dicts.LegalStopwords,
        tesauro=dicts.TesauroRevisado,
        regex_pattern=dicts.LegalRegExPatterns,
    )
    _ = model.ProcessText(txt)
    _ = model.ProcessText(txt, stemmer=True)


# EOF
