import sys

sys.path.append("src/")

exemplo_path = "./tests/exemplo.pdf"
exemplo2_path = "./tests/exemplo2.pdf"
exemplo3_path = "./tests/example-of-rotated-text-in-latex.pdf"


def test_Heuristics():
    from legal_pre_processing.heuristics import Heuristics

    _ = Heuristics(exemplo_path)
    _ = Heuristics(exemplo2_path)


def test_Heuristics_brute_txt():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo_path)
    txt_brute = model._get_brute_text()
    assert isinstance(txt_brute, str) and len(txt_brute) > 0


def test_Heuristics_txt():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo_path)
    txt = model.Extract()
    assert isinstance(txt, str) and len(txt) > 0


def test_Heuristics_txt_no_heuristics():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo_path)
    txt_brute = model._get_brute_text()
    model._preprocess_doc()
    model._apply_size_heuristics()
    model._get_text()
    assert len(model._txt) < len(txt_brute)


def test_Heuristics_txt_all_heuristics():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo_path)
    model._preprocess_doc()
    model._apply_size_heuristics()
    model._get_text()
    txt_no_heuristics = model._txt
    model = Heuristics(exemplo_path)
    model.set_all_heuristics()
    txt = model.Extract()
    assert len(txt) < len(txt_no_heuristics)


def test_Heuristics_txt_all_heuristics_2():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo2_path)
    model._preprocess_doc()
    model._apply_size_heuristics()
    model._get_text()
    txt_no_heuristics = model._txt
    model = Heuristics(exemplo2_path)
    model.set_all_heuristics()
    txt = model.Extract()
    assert len(txt) < len(txt_no_heuristics)


def test_Heuristics_txt_all_heuristics_3():
    from legal_pre_processing.heuristics import Heuristics

    model = Heuristics(exemplo3_path)
    model._preprocess_doc()
    model._apply_size_heuristics()
    model._get_text()
    txt_no_heuristics = model._txt
    model = Heuristics(exemplo3_path)
    model.set_all_heuristics()
    txt = model.Extract()
    assert len(txt) < len(txt_no_heuristics)


# EOF
