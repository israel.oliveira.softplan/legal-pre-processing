#!/bin/bash

poetry run pytest --durations=0 --cov='./src/' --cov-fail-under=80 --cov-report term --cov-report=html ./tests/

echo "file://`pwd`/htmlcov/index.html"

#EOF
